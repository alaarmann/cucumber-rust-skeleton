Feature: Example feature

  Scenario: An example scenario
    Given I am trying out Cucumber
    When I consider what I am doing
    Then I am interested in ATDD
    And we can implement rules with regex

  Scenario Outline: scenario with examples
    Given a number <num>
    Then twice that number should be <double>

  Examples:
      | num | double |
      |   2 |      4 |
      |   3 |      6 |
