use cucumber::{after, before, cucumber, steps};

#[derive(Debug)]
pub struct MyWorld {
    // You can use this struct for mutable context in scenarios.
    foo: String,
    num: Option<usize>,
    counter: Option<usize>,
}

impl cucumber::World for MyWorld {}
impl std::default::Default for MyWorld {
    fn default() -> MyWorld {
        // This function is called every time a new scenario is started
        MyWorld {
            foo: "a default string".to_string(),
            num: None,
            counter: None,
        }
    }
}

mod example_steps {
    use super::*;
    use cucumber::steps;
    use jsonpath_lib;
    use serde_json::json;
    use std::collections::HashMap;
    use std::fs;

    // Any type that implements cucumber::World + Default can be the world
    steps!(MyWorld => {
        given "I am trying out Cucumber" |world, step| {
            world.foo = "Some string".to_string();
            // Set up your context in given steps
        };

        when "I consider what I am doing" |world, step| {
            // Take actions
            let new_string = format!("{}.", &world.foo);
            world.foo = new_string;
        };

        then "I am interested in ATDD" |world, step| {
            // Check that the outcomes to be observed have occurred
            assert_eq!(world.foo, "Some string.");
        };

        then regex r"^we can (.*) rules with regex$" |world, matches, step| {
            // And access them as an array
            assert_eq!(matches[1], "implement");
        };

        then regex r"^we can also match (\d+) (.+) types$" (usize, String) |world, num, word, step| {
            // `num` will be of type usize, `word` of type String
            assert_eq!(num, 42);
            assert_eq!(word, "olika");
        };

        then "we can use data tables to provide more parameters" |world, step| {
        dbg!(world);
        dbg!(step);
        let table = step.table().unwrap().clone();

            assert_eq!(table.header, vec!["key", "value"]);

            let expected_keys = table.rows.iter().map(|row| row[0].to_owned()).collect::<Vec<_>>();
            let expected_values = table.rows.iter().map(|row| row[1].to_owned()).collect::<Vec<_>>();

            assert_eq!(expected_keys, vec!["a", "b"]);
            assert_eq!(expected_values, vec!["fizz", "buzz"]);
        };

        given regex r"^a number (\d+)$" (usize) |world, num, step| {

        world.num = Some(num);

        };

        then regex r"^twice that number should be (\d+)$" (usize) |world, double, step| {

            assert_eq!(world.num.unwrap() * 2, double);
        };

        when "I call url"  |world, step| {
        let result = api_util::send_request("https://httpbin.org/ip");

            assert_eq!(true, result.contains_key("origin"));
        };

        when "I call test server url"  |world, step| {
        let server = actix_util::start_server();
        let result = api_util::send_request("http://localhost:8088/again");


        actix_util::stop_server(server);

            assert_eq!(true, result.contains_key("message"));
        };

        when regex r"I post unique '([^']+)' to test server url" (String) |world, value, step| {
        let server = actix_util::start_server();


        let value = if let Some(counter) = world.counter {
            format!("{}{}", value, counter)
        } else {
            value
        };

        let result = do_post_request(value.as_str());

        actix_util::stop_server(server);

            assert_eq!(true, result.contains_key("message"));
            let message = result.get("message").unwrap();
            assert_eq!(true, message.to_string().contains(value.as_str()));
        };

        given "a unique counter value" |world, step| {
        let counter: usize = fetch_counter();
        world.counter = Some(counter);

        };

        then regex r"I poll every (\d+) milliseconds until message contains '([^']+)'" (usize, String) |world, interval, value, step| {
        let server = actix_util::start_server();

        let result = api_util::poll_until("http://localhost:8088/maybe", interval, |result: &HashMap<String, String>| -> bool {
            match result.get("message") {
            Some(message) => message.to_string().contains(value.as_str()),
            None => false
        }});

        actix_util::stop_server(server);

            assert_eq!(true, result.contains_key("message"));
            let message = result.get("message").unwrap();
            assert_eq!(true, message.to_string().contains(value.as_str()));
        };
    });

    fn do_post_request(value: &str) -> HashMap<String, String> {
        let path = "templates/payload_template.json";
        let template = fs::read_to_string(path).expect("Unable to read file");
        let template: serde_json::Value =
            serde_json::from_str(&template).expect("Unable to parse template");
        let payload = jsonpath_lib::replace_with(template, "$.name", &mut |_| Some(json!(value)))
            .unwrap()
            .to_string();
        println!("payload: {}", payload);
        api_util::post_request("http://localhost:8088/postme", payload)
    }

    fn fetch_counter() -> usize {
        use redis::Commands;
        let client = redis::Client::open("redis://127.0.0.1/").unwrap();
        let mut con = client.get_connection().unwrap();
        con.incr("a_counter", 1).unwrap()
    }
}

// Declares a before handler function named `a_before_fn`
before!(a_before_fn => |scenario| {
    //dbg!(scenario);

});

// Declares an after handler function named `an_after_fn`
after!(an_after_fn => |scenario| {

});

// A setup function to be called before everything else
fn setup() {}

cucumber! {
    features: "./features", // Path to our feature files
    world: crate::MyWorld, // The world needs to be the same for steps and the main cucumber call
    steps: &[
        example_steps::steps // the `steps!` macro creates a `steps` function in a module
    ],
    setup: setup, // Optional; called once before everything
    before: &[
        a_before_fn // Optional; called before each scenario
    ],
    after: &[
        an_after_fn // Optional; called after each scenario
    ]
}

mod api_util {
    use tokio::runtime::Runtime;
    use tokio::time::{delay_for, Duration};

    use reqwest::header::{HeaderValue, CONTENT_TYPE};
    use std::collections::HashMap;

    pub fn send_request(url: &str) -> HashMap<String, String> {
        Runtime::new().unwrap().block_on(call(url)).unwrap()
    }

    async fn call(url: &str) -> Result<HashMap<String, String>, Box<dyn std::error::Error>> {
        let resp = reqwest::get(url)
            .await?
            .json::<HashMap<String, String>>()
            .await?;
        println!("{:#?}", resp);
        Ok(resp)
    }

    pub fn post_request(url: &str, payload: String) -> HashMap<String, String> {
        Runtime::new()
            .unwrap()
            .block_on(post(url, payload))
            .unwrap()
    }

    async fn post(
        url: &str,
        payload: String,
    ) -> Result<HashMap<String, String>, Box<dyn std::error::Error>> {
        let resp = reqwest::Client::new()
            .post(url)
            .header(CONTENT_TYPE, HeaderValue::from_static("application/json"))
            .body(payload)
            .send()
            .await?;

        println!("{:#?}", resp);
        let resp = resp.json::<HashMap<String, String>>().await?;
        println!("{:#?}", resp);
        Ok(resp)
    }
    pub fn poll_until<F: Fn(&HashMap<String, String>) -> bool>(
        url: &str,
        interval: usize,
        predicate: F,
    ) -> HashMap<String, String> {
        (0..10)
            .map(|counter| {
                println!("Try no {}", counter);
                Runtime::new()
                    .unwrap()
                    .block_on(call_after(url, interval as u64))
                    .unwrap()
            })
            .find(predicate)
            .unwrap()
    }

    async fn call_after(
        url: &str,
        interval: u64,
    ) -> Result<HashMap<String, String>, Box<dyn std::error::Error>> {
        delay_for(Duration::from_millis(interval)).await;
        let resp = reqwest::get(url)
            .await?
            .json::<HashMap<String, String>>()
            .await?;
        println!("{:#?}", resp);
        Ok(resp)
    }
}

mod actix_util {
    use actix_web::dev::Server;
    use actix_web::{web, App, HttpServer};
    use std::sync::mpsc;
    use std::thread;

    pub fn start_server() -> Server {
        // see https://actix.rs/docs/server/

        let (tx, rx) = mpsc::channel();
        thread::spawn(move || {
            let sys = actix_rt::System::new("http-server");

            let srv = HttpServer::new(|| {
                App::new()
                    .route("/", web::get().to(cucumber_rust_skeleton::index))
                    .route("/again", web::get().to(cucumber_rust_skeleton::index2))
                    .route("/postme", web::post().to(cucumber_rust_skeleton::postme))
                    .route("/maybe", web::get().to(cucumber_rust_skeleton::maybe))
            })
            .bind("127.0.0.1:8088")
            .unwrap()
            .run();
            let _ = tx.send(srv);
            sys.run()
        });

        rx.recv().unwrap()
    }

    pub fn stop_server(srv: Server) {
        // https://github.com/actix/actix-web/issues/1270
        actix_rt::System::new("http-server").block_on(async move {
            srv.stop(true).await;
        })
    }
}
