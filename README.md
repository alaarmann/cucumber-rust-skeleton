# A Skeleton Projekt for Cucumber Rust

## Prerequisites

You need a running Redis server, e.g. `docker run -p 6379:6379 redis:6.0.7-alpine`

## Execute Tests

`cargo test`
