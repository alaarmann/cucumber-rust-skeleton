use actix_web::http;
use actix_web::{web, HttpMessage, HttpRequest, HttpResponse, Responder, Result};
use data::Payload;

pub async fn index(_req: HttpRequest) -> HttpResponse {
    match _req.content_type() {
        "text/plain" => HttpResponse::Ok().body("Hello world!"),
        _ => HttpResponse::BadRequest().body("ContentType not supported"),
    }
}

pub async fn index2(_req: HttpRequest) -> impl Responder {
    HttpResponse::Ok().body("{\"message\":\"Hello world again!\"}")
}

pub async fn postme(payload: web::Json<Payload>) -> Result<String> {
    println!("{:#?}", payload);
    Ok(format!("{{\"message\":\"Hello {}!\"}}", payload.name))
}

pub async fn maybe(_req: HttpRequest) -> Result<String> {
    use rand::Rng;
    let mut rng = rand::thread_rng();
    let random_value = rng.gen_range(0, 3);
    println!("Integer: {}", random_value);

    if random_value == 2 {
        Ok(format!("{{\"message\":\"{}\"}}", "good"))
    } else {
        Ok(format!("{{\"message\":\"{}\"}}", "bad"))
    }
}

mod data {
    use serde::Deserialize;

    #[derive(Deserialize, Debug)]
    pub struct Payload {
        pub name: String,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::test;

    #[actix_rt::test]
    async fn test_index_ok() {
        let req = test::TestRequest::with_header("content-type", "text/plain").to_http_request();
        let resp = index(req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[actix_rt::test]
    async fn test_index_not_ok() {
        let req = test::TestRequest::default().to_http_request();
        let resp = index(req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }
}
