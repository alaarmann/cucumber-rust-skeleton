use actix_web::{web, App, HttpServer};

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(cucumber_rust_skeleton::index))
            .route("/again", web::get().to(cucumber_rust_skeleton::index2))
            .route("/postme", web::post().to(cucumber_rust_skeleton::postme))
            .route("/maybe", web::get().to(cucumber_rust_skeleton::maybe))
    })
    .bind("127.0.0.1:8088")?
    .run()
    .await
}
